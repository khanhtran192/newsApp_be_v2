package com.newsApp_be.controller;

import com.newsApp_be.dto.request.CreateDiscussionDTO;
import com.newsApp_be.dto.request.CreatePostDTO;
import com.newsApp_be.services.DiscussionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class DiscussionController {

    @Autowired
    private DiscussionService discussionService;


    @PostMapping("/user/discussion")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createDiscussion(@RequestBody CreateDiscussionDTO created){
        return discussionService.createDiscussion(created);
    }

    @GetMapping("/public/discussion")
    public ResponseEntity<?> listDiscussion(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                            @RequestParam(value = "postId") Long postId){
        return discussionService.listDiscussion(postId ,PageRequest.of(page-1, 10,
                Sort.by("createdTime").descending()));
    }
}
