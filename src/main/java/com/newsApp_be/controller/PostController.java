package com.newsApp_be.controller;

import com.newsApp_be.dto.request.CreateCommentDTO;
import com.newsApp_be.dto.request.CreatePostDTO;
import com.newsApp_be.services.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping("/user/post")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createPost(@RequestBody CreatePostDTO created){
        return postService.createPost(created);
    }

    @GetMapping("/public/posts/post")
    public ResponseEntity<?> post(@RequestParam("postId") Long id){
        return postService.post(id);
    }

    @GetMapping("/public/posts")
    public ResponseEntity<?> listPost(@RequestParam(value = "page",defaultValue = "1") Integer page){
        return postService.listPost(PageRequest.of(page-1, 10,
                Sort.by("createdTime").descending()));
    }
}
