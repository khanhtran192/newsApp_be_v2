package com.newsApp_be.controller;

import com.newsApp_be.dto.response.NewsDTO;
import com.newsApp_be.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/category")
    public ResponseEntity<?> pageCategory(@RequestParam("page")int page){
        return new ResponseEntity<>(categoryService.listCategories(PageRequest.of((page-1), 25,
                Sort.by("name").descending())), HttpStatus.OK);
    }
}
