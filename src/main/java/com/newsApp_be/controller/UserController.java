package com.newsApp_be.controller;

import com.newsApp_be.dto.request.FileDTO;
import com.newsApp_be.dto.request.UserInfoDTO;
import com.newsApp_be.services.UserService;
import com.newsApp_be.services.impl.MinioService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private MinioService minioService;

    @GetMapping("/user/info")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getUserInfo() {
        return userService.getUserInfo();
    }

    @PutMapping("/user/update")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateUserInfo(@RequestBody UserInfoDTO userInfoDTO) {
        return userService.updateUserInfo(userInfoDTO);
    }

    @GetMapping("/public/user")
    public ResponseEntity<?> getUserInfo2(@RequestParam("userId") Long userId) {
        return userService.getUserInfo2(userId);
    }

    @PostMapping("/user/upload-avt")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadAvatar(@ModelAttribute FileDTO request,
                                          @RequestParam("userId") Long userId) {
        return userService.uploadAvatar(request, userId);
    }


}
