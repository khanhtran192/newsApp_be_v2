package com.newsApp_be.controller;

import com.newsApp_be.dto.request.ChangePasswordByEmailDTO;
import com.newsApp_be.dto.request.ChangePasswordDTO;
import com.newsApp_be.dto.request.VerifyEmailRequest;
import com.newsApp_be.security.request.SignupRequest;
import com.newsApp_be.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    private final UserService userService;

    @PostMapping("/rigister")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        return userService.registerUser(signupRequest);
    }

    @PostMapping("/verify")
    public ResponseEntity<?> verifyEmail(@Valid @RequestBody VerifyEmailRequest request) {
        return userService.verifyEmail(request);
    }

    @PostMapping("/change-password")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordDTO request) {
        return userService.changePassword(request);
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestParam(name = "email") String email) {
        return userService.forgotPassword(email);
    }

    @PostMapping("/forgot-password/verify")
    public ResponseEntity<?> verifyForgotPassword(@RequestBody VerifyEmailRequest req) {
        return userService.verifyForgotPassword(req);
    }

    @PostMapping("/forgot-password/new-password")
    public ResponseEntity<?> newPassword(@Valid @RequestBody ChangePasswordByEmailDTO request) {
        return userService.changePassword(request);
    }
}
