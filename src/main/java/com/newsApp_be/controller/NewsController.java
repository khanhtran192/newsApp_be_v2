package com.newsApp_be.controller;


import com.newsApp_be.dto.response.NewsDTO;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.dto.response.ResponseNewsDTO;
import com.newsApp_be.services.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public")
public class NewsController {
    @Autowired
    private NewsService newsService;

    @GetMapping("/news")
    public ResponseEntity<?> listNewsLatest(@RequestParam(value = "page",defaultValue = "1") Integer page){
        try {
            ResponseNewsDTO listNews = newsService.listNewsLatest(PageRequest.of(page-1, 25,
                    Sort.by("createdAt").descending()));
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .message("Danh sách các bài báo mới nhất")
                    .status(true)
                    .statusCode(200L)
                    .data(listNews).build();
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }catch (Exception e){
        }
        ResponseDTO responseDTO = ResponseDTO.builder()
                .message("Rất tiếc đã xảy ra lỗi")
                .status(false)
                .statusCode(400L)
                .build();
        return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/news-by-category")
    public ResponseEntity<?> listNewsLatestByCategory(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                            @RequestParam(value = "category") Long category){
        try {
            ResponseNewsDTO listNews = newsService.listNewsLatestByCategory(category, PageRequest.of(page-1, 25,
                    Sort.by("createdAt").descending()));
            return new ResponseEntity<>(ResponseDTO.builder()
                    .message("danh sách các bài báo mới nhất theo category")
                    .status(true)
                    .data(listNews)
                    .build()
                    , HttpStatus.OK);

        }catch (Exception e){}
        return new ResponseEntity<>(ResponseDTO.builder()
                .message("Rất tiếc đã xảy ra lỗi")
                .status(false).build(), HttpStatus.OK);
    }

    @GetMapping("/news-trending")
    public ResponseEntity<?> listNewsTrending(@RequestParam(value = "page",defaultValue = "1") Integer page){
        ResponseNewsDTO listNews = newsService.listNewsTrending(PageRequest.of(page-1, 5,
                Sort.by("views").descending()));
        return new ResponseEntity<>(listNews, HttpStatus.OK);
    }

    @GetMapping("/news-trending-by-category")
    public ResponseEntity<?> listNewsTrendingByCategory(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                                      @RequestParam(value = "category") Long category){
        ResponseNewsDTO listNews = newsService.listNewsTrendingByCategory(category, PageRequest.of(page-1, 5,
                Sort.by("views").descending()));
        return new ResponseEntity<>(listNews, HttpStatus.OK);
    }

    @GetMapping("/news/{id}")
    public ResponseEntity<?> getNews(@PathVariable("id") Long id){
        return new ResponseEntity<>(newsService.news(id), HttpStatus.OK);
    }



}
