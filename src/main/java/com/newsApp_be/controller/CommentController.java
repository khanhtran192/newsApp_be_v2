package com.newsApp_be.controller;


import com.newsApp_be.dto.request.CreateCommentDTO;
import com.newsApp_be.dto.request.CreateReCommentDTO;
import com.newsApp_be.services.CommentService;
import com.newsApp_be.services.RecommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private RecommentService recommentService;

    @GetMapping("/public/comments")
    public ResponseEntity<?> listComment(@RequestParam("newsId") Long id){
        return new ResponseEntity<>(commentService.listComment(id), HttpStatus.OK);
    }

    @GetMapping("/public/comments/{id}")
    public ResponseEntity<?> comment(@PathVariable("id") Long id){
        return new ResponseEntity<>(commentService.commentResponse(id), HttpStatus.OK);
    }


    @GetMapping("/public/comments/recomment")
    public ResponseEntity<?> recomment(@RequestParam("commentId") Long id){
        return new ResponseEntity<>(recommentService.listReComments(commentService.comment(id)), HttpStatus.OK);
    }

    @PostMapping("/user/comment")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createComment(@RequestBody CreateCommentDTO created){
        return new ResponseEntity<>(commentService.createComment(created), HttpStatus.OK);
    }

    @PostMapping("/user/recomment")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createReComment(@RequestBody CreateReCommentDTO createReCommentDTO){
        return new ResponseEntity<>(recommentService.createReComment(createReCommentDTO),HttpStatus.OK);
    }
}
