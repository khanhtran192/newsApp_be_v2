package com.newsApp_be.dto.response;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class NewsDTO {
    private Long id;
    private String title;
    private String description;
    private String image;
    private String content;
    private String slug;
    private String authorId;
    private Timestamp createdAt;
    private Timestamp updateAt;
    private String category;
    private int views;
}
