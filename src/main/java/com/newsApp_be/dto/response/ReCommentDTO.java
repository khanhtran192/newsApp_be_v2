package com.newsApp_be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReCommentDTO {
    private Long id;
    private String content;
    private UserDTO user;
    private Long commentId;
}
