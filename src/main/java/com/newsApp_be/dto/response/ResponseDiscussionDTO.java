package com.newsApp_be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDiscussionDTO {
    private Long totalPage;
    private List<DiscussionResponseDTO> discussion;
}
