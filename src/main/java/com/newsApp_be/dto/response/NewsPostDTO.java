package com.newsApp_be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewsPostDTO {
    private String title;
    private String image;
    private String description;
    private Long id;
}
