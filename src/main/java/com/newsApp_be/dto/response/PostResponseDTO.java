package com.newsApp_be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostResponseDTO {
    private Long id;
    private UserDTO user;
    private String content;
    private String link;
    private String image;
    private Date createTime;
    private Long discussionTotal;
    private NewsPostDTO news;
}
