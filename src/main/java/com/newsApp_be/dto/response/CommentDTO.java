package com.newsApp_be.dto.response;

import com.newsApp_be.entity.ReComment;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;


@Data
@Builder
public class CommentDTO {
    private Long id;
    private String content;
    private Date createdTime;
    private UserDTO user;
//    Set<ReComment> reComment;
    private Integer count;

}
