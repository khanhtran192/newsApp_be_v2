package com.newsApp_be.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerifyForgotPassword {
    private String email;
    private String message;
    private Integer statusCode;

}
