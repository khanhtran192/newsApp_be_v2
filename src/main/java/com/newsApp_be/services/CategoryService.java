package com.newsApp_be.services;

import com.newsApp_be.dto.response.CategoryDTO;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.entity.NewsCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface CategoryService {
    ResponseDTO listCategories(Pageable pageable);

    CategoryDTO mapCategory(NewsCategory category);
}
