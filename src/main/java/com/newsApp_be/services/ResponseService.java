package com.newsApp_be.services;

import com.newsApp_be.dto.response.ResponseDTO;
import org.springframework.stereotype.Service;

@Service
public interface ResponseService {
    ResponseDTO mapData(Object object, String msg);
}
