package com.newsApp_be.services;

import com.newsApp_be.controller.CommentController;
import com.newsApp_be.dto.request.CreateReCommentDTO;
import com.newsApp_be.dto.response.CommentDTO;
import com.newsApp_be.dto.response.ReCommentDTO;
import com.newsApp_be.entity.Comment;
import com.newsApp_be.entity.ReComment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RecommentService {
    CommentDTO createReComment(CreateReCommentDTO create);

    List<ReCommentDTO> listReComments(Comment comment);

    ReCommentDTO mapRecomment(ReComment reComment);





}
