package com.newsApp_be.services;

import com.newsApp_be.dto.request.CreatePostDTO;
import com.newsApp_be.dto.response.PostResponseDTO;
import com.newsApp_be.entity.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface PostService {
    ResponseEntity<?> listPost(Pageable pageable);
    ResponseEntity<?> post(Long id);
    ResponseEntity<?> createPost(CreatePostDTO createPostDTO);

    PostResponseDTO mapPost(Post post);

}
