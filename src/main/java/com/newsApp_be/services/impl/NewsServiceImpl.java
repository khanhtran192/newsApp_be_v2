package com.newsApp_be.services.impl;

import com.newsApp_be.dto.response.NewsDTO;
import com.newsApp_be.dto.response.NewsPostDTO;
import com.newsApp_be.dto.response.ResponseNewsDTO;
import com.newsApp_be.entity.News;
import com.newsApp_be.entity.NewsCategory;
import com.newsApp_be.repository.CategoryRepository;
import com.newsApp_be.repository.NewsRepository;
import com.newsApp_be.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public ResponseNewsDTO listNewsLatest(Pageable pageable) {
        Page<News> listNews = newsRepository.listNews("publish", pageable);
        return ResponseNewsDTO.builder()
                .news(mapListNewsDTO(listNews))
                .totalPage(listNews.getTotalPages()).build();

    }

    @Override
    public ResponseNewsDTO listNewsLatestByCategory(Long category, Pageable pageable) {
        Optional<NewsCategory> newsCategory = categoryRepository.findById(category);
        Page<News> listNews = newsRepository.listNewsByCategory("publish" ,newsCategory.get(), pageable);
        return ResponseNewsDTO.builder()
                .news(mapListNewsDTO(listNews))
                .totalPage(listNews.getTotalPages()).build();


    }

    @Override
    public ResponseNewsDTO listNewsTrending(Pageable pageable) {
        Page<News> listNews = newsRepository.listNewsTrending("publish", pageable);
        return ResponseNewsDTO.builder()
                .news(mapListNewsDTO(listNews))
                .totalPage(listNews.getTotalPages()).build();
    }

    @Override
    public ResponseNewsDTO listNewsTrendingByCategory(Long category, Pageable pageable) {
        Optional<NewsCategory> newsCategory = categoryRepository.findById(category);
        Page<News> listNews = newsRepository.listNewsTrendingByCategory("publish" ,newsCategory.get(), pageable);
        return ResponseNewsDTO.builder()
                .news(mapListNewsDTO(listNews))
                .totalPage(listNews.getTotalPages()).build();
    }

    @Override
    public List<NewsDTO> mapListNewsDTO(Page<News> list) {
        List<NewsDTO> listNewsDTO = new ArrayList<>();

        for (News news : list) {
            NewsDTO newsDTO = NewsDTO.builder()
                    .id(news.getId())
                    .slug(news.getSlug())
                    .title(news.getTitle())
                    .description(news.getDescription())
                    .image(news.getImage())
                    .authorId(news.getAuthorId())
                    .createdAt(news.getCreatedAt())
                    .updateAt(news.getUpdateAt())
                    .content(news.getContent())
                    .category(news.getCategory().getName())
                    .build();
            listNewsDTO.add(newsDTO);
        }
        return listNewsDTO;
    }

    @Override
    public void updateView(Long id) {
        Optional<News> news = newsRepository.findById(id);
        int newView = news.get().getViews() + 1;
        news.get().setViews(newView);
        newsRepository.save(news.get());
    }

    @Override
    public NewsDTO news(Long id) {
        News news = newsRepository.findById(id).get();
        updateView(id);
        return NewsDTO.builder()
                .id(news.getId())
                .slug(news.getSlug())
                .title(news.getTitle())
                .description(news.getDescription())
                .image(news.getImage())
                .authorId(news.getAuthorId())
                .createdAt(news.getCreatedAt())
                .updateAt(news.getUpdateAt())
                .content(news.getContent())
                .category(news.getCategory().getName())
                .views(news.getViews())
                .build();
    }

    @Override
    public NewsPostDTO newsPost(Long newsId){
        if(newsId != null){
            News news = newsRepository.findById(newsId).get();
            return NewsPostDTO.builder()
                    .description(news.getDescription())
                    .id(news.getId())
                    .image(news.getImage())
                    .title(news.getTitle()).build();
        }return null;
    }
}
