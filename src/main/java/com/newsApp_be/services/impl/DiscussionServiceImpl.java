package com.newsApp_be.services.impl;

import com.newsApp_be.dto.request.CreateDiscussionDTO;
import com.newsApp_be.dto.response.DiscussionResponseDTO;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.dto.response.ResponseDiscussionDTO;
import com.newsApp_be.dto.response.ResponsePostDTO;
import com.newsApp_be.entity.Discussion;
import com.newsApp_be.repository.DiscussionRepository;
import com.newsApp_be.repository.PostRepository;
import com.newsApp_be.repository.UserRepository;
import com.newsApp_be.services.DiscussionService;
import com.newsApp_be.services.PostService;
import com.newsApp_be.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class DiscussionServiceImpl implements DiscussionService {

    @Autowired
    private DiscussionRepository discussionRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;



    @Override
    public ResponseEntity<?> createDiscussion(CreateDiscussionDTO create) {
        try {
            Discussion discussion = Discussion.builder()
                    .content(create.getContent())
                    .createdTime(new Timestamp(System.currentTimeMillis()))
                    .post(postRepository.findById(create.getPostId()).get())
                    .userDiscussion(userRepository.findById(create.getUserId()).get())
                    .build();
            discussionRepository.save(discussion);
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(200L)
                    .status(true)
                    .message("Thêm thảo luận thành công")
                    .build()
                    , HttpStatus.OK
            );
        }catch (Exception e){}
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(400L)
                .status(true)
                .message("Xảy ra lỗi khi tạo")
                .build()
                , HttpStatus.BAD_REQUEST
        );
    }

    @Override
    public ResponseEntity<?> listDiscussion(Long postId, Pageable pageable) {
        try{
            Page<Discussion> page = discussionRepository.findByPost(postRepository.findById(postId).get(),
                    pageable);
            List<DiscussionResponseDTO> list = new ArrayList<>();
            for (Discussion discussion : page) {
                list.add(mapDiscussion(discussion));
            }
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(200L)
                    .status(true)
                    .message("thảo luận")
                    .data(ResponseDiscussionDTO.builder()
                            .discussion(list)
                            .totalPage((long) page.getTotalPages())
                            .build())
                    .build()
                    , HttpStatus.OK
            );
        }catch (Exception e){}
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(400L)
                .status(false)
                .message("Lỗi")
                .build()
                , HttpStatus.BAD_REQUEST
        );

    }

    @Override
    public DiscussionResponseDTO mapDiscussion(Discussion discussion) {
        return DiscussionResponseDTO.builder()
                .id(discussion.getId())
                .content(discussion.getContent())
                .createTime(discussion.getCreatedTime())
                .user(userService.mapUserDTO(discussion.getUserDiscussion()))
                .postId(discussion.getPost().getId())
                .build();
    }
}
