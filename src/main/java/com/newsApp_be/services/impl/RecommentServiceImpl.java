package com.newsApp_be.services.impl;

import com.newsApp_be.dto.request.CreateReCommentDTO;
import com.newsApp_be.dto.response.CommentDTO;
import com.newsApp_be.dto.response.ReCommentDTO;
import com.newsApp_be.entity.Comment;
import com.newsApp_be.entity.ReComment;
import com.newsApp_be.repository.CommentRepository;
import com.newsApp_be.repository.RecommentRepository;
import com.newsApp_be.repository.UserRepository;
import com.newsApp_be.services.CommentService;
import com.newsApp_be.services.RecommentService;
import com.newsApp_be.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class RecommentServiceImpl implements RecommentService {

    @Autowired
    private RecommentRepository recommentRepository;

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Override
    public CommentDTO createReComment(CreateReCommentDTO create) {

        ReComment reComment = ReComment.builder()
                .comment(commentRepository.findById(create.getCommentId()).get())
                .content(create.getContent())
                .createdTime(new Timestamp(System.currentTimeMillis()))
                .userCreator(userRepository.findById(create.getCommentId()).get()).build();
        recommentRepository.save(reComment);
        return commentService.mapContenDTO(reComment.getComment());

    }

    @Override
    public List<ReCommentDTO> listReComments(Comment comment) {
        List<ReCommentDTO> list = new ArrayList<>();
        List<ReComment> listReComments = recommentRepository.findByComment(comment);
        for (ReComment reComment : listReComments) {
            list.add(mapRecomment(reComment));
        }
        return list;
    }

    @Override
    public ReCommentDTO mapRecomment(ReComment reComment) {
        return ReCommentDTO.builder()
                .commentId(reComment.getComment().getId())
                .id(reComment.getId())
                .content(reComment.getContent())
                .user(userService.mapUserDTO(reComment.getUserCreator())).build();
    }


}
