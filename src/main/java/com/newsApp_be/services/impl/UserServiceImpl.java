package com.newsApp_be.services.impl;

import com.newsApp_be.dto.request.*;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.dto.response.UserDTO;
import com.newsApp_be.dto.response.UserResponseDTO;
import com.newsApp_be.dto.response.VerifyForgotPassword;
import com.newsApp_be.exception.BadRequestException;
import com.newsApp_be.entity.ERole;
import com.newsApp_be.entity.User;
import com.newsApp_be.repository.UserRepository;
import com.newsApp_be.security.jwt.JwtUtils;
import com.newsApp_be.security.request.LoginRequest;
import com.newsApp_be.security.request.SignupRequest;
import com.newsApp_be.security.response.JwtResponse;
import com.newsApp_be.security.response.MessageResponse;
import com.newsApp_be.security.response.ResponseStatus;
import com.newsApp_be.security.service.UserDetailsImpl;
import com.newsApp_be.services.UserService;
import com.newsApp_be.utils.ThreadSendEmailOTP;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    private final PasswordEncoder encoder;

    private final ThreadSendEmailOTP threadSendEmailOTP;

    private final UserRepository userRepository;

    @Autowired
    private MinioService minioService;


    @Override
    public ResponseEntity<?> authenticateUser(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        User user = userRepository.findByEmail(userDetails.getEmail()).get();
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getUsername(),
                user.getFullName(),
                user.getId(),
                userDetails.getEmail(),
                roles,
                200L,
                true));
    }

    @Override
    public ResponseEntity<?> registerUser(SignupRequest signupRequest) {
//        if (userRepository.existsByUsernameAndEmailAndEnable(signupRequest.getUsername(), signupRequest.getEmail(), true)) {
//            return new ResponseEntity<>(ResponseDTO.builder()
//                    .statusCode(4001L)
//                    .status(false)
//                    .message("Username hoặc email đã tồn tại!").build(), HttpStatus.BAD_REQUEST);
//        }

        if(userRepository.existsByEmailAndEnable(signupRequest.getEmail(), true)){
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(4001L)
                    .status(false)
                    .message("email đã tồn tại!").build(), HttpStatus.BAD_REQUEST);
        }
        if(userRepository.existsByUsernameAndEnable(signupRequest.getUsername(), true)){
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(4001L)
                    .status(false)
                    .message("username đã tồn tại!").build(), HttpStatus.BAD_REQUEST);
        }


        User user;
//        int randomCode = 1902;
        Random random = new Random();
        int randomCode = random.nextInt(100000, 999999);
        if (Boolean.TRUE.equals(userRepository.existsByEmailAndEnable(signupRequest.getEmail(), false))) {
            user = userRepository.findByEmail(signupRequest.getEmail())
                    .orElseThrow(() -> new BadRequestException("Tài khoản chưa xác thực"));
            user.setPassword(encoder.encode(signupRequest.getPassword()));
            user.setVerifyCode(String.valueOf(randomCode));
        } else {
            user = User.builder()
                    .username(signupRequest.getUsername())
                    .email(signupRequest.getEmail())
                    .fullName(signupRequest.getFullname())
                    .createdTime(new Timestamp(System.currentTimeMillis()))
                    .verifyCode(String.valueOf(randomCode))
                    .email(signupRequest.getEmail())
                    .password(encoder.encode(signupRequest.getPassword()))
                    .role(ERole.ROLE_USER)
                    .enable(false).build();
        }
        userRepository.save(user);
        threadSendEmailOTP.start(signupRequest.getEmail(), String.valueOf(randomCode));
        return new ResponseEntity<>(
                ResponseDTO.builder()
                        .statusCode(200L)
                        .status(true)
                        .message("Mã xác thực đã được gửi. Vui lòng xác thực tài khoản").build()
                , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> verifyEmail(VerifyEmailRequest request) {
        if (userRepository.existsByEmailAndEnable(request.getEmail(), true)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse<>("Lỗi: Email đã được sử dụng", ResponseStatus.FAIL.getValue()));
        }
        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new BadRequestException("Lỗi: Email không tồn tại"));
        String code = Objects.isNull(user.getVerifyCode()) ? "0" : user.getVerifyCode();
        if (code.equals(request.getCode())) {
            user.setEnable(true);
            user.setVerifyCode(null);
            userRepository.save(user);
            return ResponseEntity.ok()
                    .body(new MessageResponse<>("Xác thực email thành công",
                            ResponseStatus.SUCCESS.getValue(),
                            Map.of("email", request.getEmail())));
        }
        return ResponseEntity.badRequest()
                .body(new MessageResponse<>("Sai mã xác thực hoặc mã xác thực hết thời hạn",
                        ResponseStatus.FAIL.getValue()));
    }


    @Override
    public ResponseEntity<?> getUserInfo() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByUsernameAndEnable(email, true)
                .orElseThrow(() -> new BadRequestException("Lỗi: Email không tồn tại hoặc chưa được kích hoạt"));
        UserInfoDTO userInfoDTO = user.toUserInfoDTO();
        if (!StringUtils.isEmpty(user.getAvatar())){
            userInfoDTO.setAvatar("");
        }
        userInfoDTO.setAvatar(minioService.getObject(user.getAvatar()));
        return ResponseEntity.ok()
                .body(new MessageResponse<>(ResponseStatus.SUCCESS.getValue(), userInfoDTO));
    }

    @Override
    public ResponseEntity<?> getUserInfo2(Long userId) {
        User user = userRepository.findById(userId).get();
        UserResponseDTO userResponseDTO = UserResponseDTO.builder()
                .fullname(user.getFullName())
                .avatar(minioService.getObject(user.getAvatar())).build();
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(200L)
                .status(true)
                .message("Thông tin người dùng")
                .data(userResponseDTO).build(),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> updateUserInfo(UserInfoDTO request) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByUsernameAndEnable(email, true)
                .orElseThrow(() -> new BadRequestException("Lỗi: Email không tồn tại hoặc chưa được kích hoạt"));
        if (!StringUtils.isEmpty(request.getAvatar())) {
            user.setAvatar(request.getAvatar());
        }
        user.setFullName(request.getFullName());
        user.setPhoneNumber(request.getPhoneNumber());
        user.setBirthDay(request.getBirthDay());
        user.setSex(request.getSex());
        user.setAddress(request.getAddress());
        userRepository.save(user);
        return ResponseEntity.ok()
                .body(new MessageResponse<>("Xác thực thông tin thành công",
                        ResponseStatus.SUCCESS.getValue(),
                        user.toUserInfoDTO()));
    }


    @Override
    public ResponseEntity<?> changePassword(ChangePasswordDTO request) {
        if (!userRepository.existsByUsernameAndEnable(request.getUsername(),  true)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse<>("Lỗi: Không tìm thấy user!", ResponseStatus.FAIL.getValue()));
        }
        User user = userRepository.findByUsernameAndEnable(request.getUsername(), true)
                .orElseThrow(() -> new BadRequestException("Lỗi: user chưa được kích hoạt"));
        user.setPassword(encoder.encode(request.getPassword()));
        userRepository.save(user);
        return ResponseEntity.ok()
                .body(new MessageResponse<>("Thay đổi mật khẩu thành công", ResponseStatus.SUCCESS.getValue()));
    }

    @Override
    public UserDTO mapUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .fullname(user.getFullName())
                .avatar(user.getAvatar()).build();
    }

    @Override
    public ResponseEntity<?> uploadAvatar(FileDTO file, Long userId) {
        try{
            String image = minioService.uploadFile(file);
            User user = userRepository.findById(userId).get();
            user.setAvatar(image);
            userRepository.save(user);
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(200L)
                    .message("Upload thành công")
                    .status(true)
                    .build(), HttpStatus.OK
            );

        }catch (Exception e){

        }return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(400L)
                .message("Upload thất bại")
                .status(false)
                .build(), HttpStatus.BAD_REQUEST
        );
    }

    @Override
    public ResponseEntity<?> forgotPassword(String email) {
        if(!userRepository.existsByEmailAndEnable(email, true)){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse<>("Lỗi: Email không tồn tại", ResponseStatus.FAIL.getValue()));
        }
        Random random = new Random();
        int randomCode = random.nextInt(100000, 999999);
        User user = userRepository.findByEmailAndEnable(email, true).get();
        user.setVerifyCode(String.valueOf(randomCode));
        userRepository.save(user);
        threadSendEmailOTP.start(email, String.valueOf(randomCode));
        return new ResponseEntity<>(
                ResponseDTO.builder()
                        .statusCode(200L)
                        .status(true)
                        .message("Mã xác thực đã được gửi. Vui lòng xác thực tài khoản").build()
                , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> verifyForgotPassword(VerifyEmailRequest request) {
        if(!userRepository.existsByEmailAndEnable(request.getEmail(), true)){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse<>("Lỗi: Email không tồn tại", ResponseStatus.FAIL.getValue()));
        }
        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new BadRequestException("Lỗi: Email không tồn tại"));
        String code = Objects.isNull(user.getVerifyCode()) ? "0" : user.getVerifyCode();
        if (code.equals(request.getCode())) {
            user.setVerifyCode(null);
            userRepository.save(user);
            return ResponseEntity.ok()
                    .body(VerifyForgotPassword.builder()
                            .message("Xác thực thành công")
                            .statusCode(200)
                            .email(user.getEmail()).build());
        }
        return ResponseEntity.ok()
                .body(VerifyForgotPassword.builder()
                        .message("Sai mã xác thực hoặc mã xác thực hết thời hạn")
                        .statusCode(400)
                        .build());
    }

    @Override
    public ResponseEntity<?> changePassword(ChangePasswordByEmailDTO request) {
        if (!userRepository.existsByEmailAndEnable(request.getEmail(),  true)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse<>("Lỗi: Không tìm thấy user!", ResponseStatus.FAIL.getValue()));
        }
        User user = userRepository.findByEmailAndEnable(request.getEmail(), true)
                .orElseThrow(() -> new BadRequestException("Lỗi: user chưa được kích hoạt"));
        user.setPassword(encoder.encode(request.getPassword()));
        userRepository.save(user);
        return ResponseEntity.ok()
                .body(new MessageResponse<>("Thay đổi mật khẩu thành công", ResponseStatus.SUCCESS.getValue()));
    }


}
