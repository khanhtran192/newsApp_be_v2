package com.newsApp_be.services.impl;

import com.newsApp_be.dto.request.CreatePostDTO;
import com.newsApp_be.dto.response.PostResponseDTO;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.dto.response.ResponsePostDTO;
import com.newsApp_be.entity.Post;
import com.newsApp_be.repository.DiscussionRepository;
import com.newsApp_be.repository.PostRepository;
import com.newsApp_be.repository.UserRepository;
import com.newsApp_be.services.NewsService;
import com.newsApp_be.services.PostService;
import com.newsApp_be.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private UserService userService;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private DiscussionRepository discussionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NewsService newsService;


    @Override
    public ResponseEntity<?> listPost(Pageable pageable) {
        Page<Post> page = postRepository.listPost(pageable);
        List<PostResponseDTO> list = new ArrayList<>();
        for (Post post : page) {
            list.add(mapPost(post));
        }
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(200L)
                .status(true)
                .message("danh sách thảo luận")
                .data(ResponsePostDTO.builder()
                        .post(list)
                        .totalPage((long) page.getTotalPages())
                        .build())
                .build()
                , HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<?> post(Long id) {
        Post post = postRepository.findById(id).get();
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(200L)
                .status(true)
                .message("Thảo luận")
                .data(mapPost(post))
                .build()
                , HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<?> createPost(CreatePostDTO createPostDTO) {
        try{
            Post post = Post.builder()
                    .content(createPostDTO.getContent())
                    .userPost(userRepository.findById(createPostDTO.getUserId()).get())
                    .newsId(createPostDTO.getNewsId())
                    .createdTime(new Timestamp(System.currentTimeMillis()))
                    .build();
            postRepository.save(post);
            return new ResponseEntity<>(ResponseDTO.builder()
                    .statusCode(200L)
                    .status(true)
                    .message("Tạo chủ đề thảo luận thành công")
                    .build()
                    , HttpStatus.OK
            );
        }catch (Exception e){
        }
        return new ResponseEntity<>(ResponseDTO.builder()
                .statusCode(400L)
                .status(true)
                .message("Xảy ra lỗi khi tạo")
                .build()
                , HttpStatus.BAD_REQUEST
        );
    }

    @Override
    public PostResponseDTO mapPost(Post post) {
        long discussionTotal = 0;
        if (discussionRepository.findDiscussionByPost(post) != null) discussionTotal = discussionRepository.findDiscussionByPost(post).size();
        return PostResponseDTO.builder()
                .id(post.getId())
                .content(post.getContent())
                .createTime(post.getCreatedTime())
                .image(post.getImage())
                .user(userService.mapUserDTO(post.getUserPost()))
                .discussionTotal(discussionTotal)
                .news(newsService.newsPost(post.getNewsId()))
                .build();
    }
}
