package com.newsApp_be.services.impl;

import com.newsApp_be.dto.response.CategoryDTO;
import com.newsApp_be.dto.response.ResponseDTO;
import com.newsApp_be.entity.NewsCategory;
import com.newsApp_be.repository.CategoryRepository;
import com.newsApp_be.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Override
    public ResponseDTO listCategories(Pageable pageable) {
        Page<NewsCategory> pageData = categoryRepository.pageCategory(pageable);
        List<CategoryDTO> list = new ArrayList<>();
        for (NewsCategory item : pageData) {
            list.add(mapCategory(item));
        }
        return ResponseDTO.builder()
                .message("danh sách thể loại")
                .status(true)
                .statusCode(200L)
                .data(list).build();
    }

    @Override
    public CategoryDTO mapCategory(NewsCategory category) {
        return CategoryDTO.builder()
                .id(category.getId())
                .name(category.getName())
                .build();
    }
}
