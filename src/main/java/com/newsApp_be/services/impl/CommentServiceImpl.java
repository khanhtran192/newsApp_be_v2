package com.newsApp_be.services.impl;

import com.newsApp_be.dto.request.CreateCommentDTO;
import com.newsApp_be.dto.response.CommentDTO;
import com.newsApp_be.entity.Comment;
import com.newsApp_be.entity.News;
import com.newsApp_be.entity.ReComment;
import com.newsApp_be.repository.CommentRepository;
import com.newsApp_be.repository.NewsRepository;
import com.newsApp_be.repository.UserRepository;
import com.newsApp_be.services.CommentService;
import com.newsApp_be.services.RecommentService;
import com.newsApp_be.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private NewsRepository newsRepository;

    @Override
    public CommentDTO createComment(CreateCommentDTO createCommentDTO) {
        Comment comment = Comment.builder()
                .createdTime(new Timestamp(System.currentTimeMillis()))
                .content(createCommentDTO.getContent())
                .userCreator(userRepository.findById(createCommentDTO.getUserId()).get())
                .news(newsRepository.findById(createCommentDTO.getNewsId()).get())
                .build();
        commentRepository.save(comment);
        return mapContenDTO(comment);
    }

    @Override
    public CommentDTO mapContenDTO(Comment comment) {
        int countRecomment = 0;
        if (comment.getListReComment() != null){
            countRecomment = comment.getListReComment().size();
        }
        return CommentDTO.builder()
                .id(comment.getId())
                .user(userService.mapUserDTO(comment.getUserCreator()))
                .content(comment.getContent())
                .createdTime(comment.getCreatedTime())
                .count(countRecomment)
//                .reComment(comment.getListReComment())
                .build();
    }

    @Override
    public List<CommentDTO> listComment(Long id) {
        List<CommentDTO> listCommentsDTO = new ArrayList<>();
        News news = newsRepository.findById(id).get();
        List<Comment> listComment = commentRepository.listComment(news);
        for (Comment comment : listComment) {
            listCommentsDTO.add(mapContenDTO(comment));
        }
        return listCommentsDTO;
    }

    @Override
    public CommentDTO commentResponse(Long id) {
        return mapContenDTO(commentRepository.findById(id).get());
    }

    @Override
    public Comment comment(Long id) {
        return commentRepository.findById(id).get();
    }


}
