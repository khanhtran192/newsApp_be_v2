package com.newsApp_be.services;

import com.newsApp_be.dto.request.*;
import com.newsApp_be.dto.response.UserDTO;
import com.newsApp_be.entity.User;
import com.newsApp_be.security.request.LoginRequest;
import com.newsApp_be.security.request.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface UserService {

    ResponseEntity<?> authenticateUser(LoginRequest loginRequest);
    ResponseEntity<?> registerUser(SignupRequest signupRequest);
    ResponseEntity<?> verifyEmail(VerifyEmailRequest verifyEmailRequest);

    ResponseEntity<?> getUserInfo();

    ResponseEntity<?> getUserInfo2(Long userId);

    ResponseEntity<?> updateUserInfo(UserInfoDTO request);

    ResponseEntity<?> changePassword(ChangePasswordDTO request);

    UserDTO mapUserDTO(User user);

    ResponseEntity<?> uploadAvatar(FileDTO file, Long userId);

    ResponseEntity<?> forgotPassword(String email);

    ResponseEntity<?> verifyForgotPassword(VerifyEmailRequest request);

    ResponseEntity<?> changePassword(ChangePasswordByEmailDTO request);


}
