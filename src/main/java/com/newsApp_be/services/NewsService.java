package com.newsApp_be.services;

import com.newsApp_be.dto.response.NewsDTO;
import com.newsApp_be.dto.response.NewsPostDTO;
import com.newsApp_be.dto.response.ResponseNewsDTO;
import com.newsApp_be.entity.News;
import com.newsApp_be.entity.NewsCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NewsService {
    ResponseNewsDTO listNewsLatest(Pageable pageable);
    ResponseNewsDTO listNewsLatestByCategory(Long category, Pageable pageable);
    ResponseNewsDTO listNewsTrending(Pageable pageable);
    ResponseNewsDTO listNewsTrendingByCategory(Long category, Pageable pageable);



    List<NewsDTO> mapListNewsDTO(Page<News> list);



    void updateView(Long id);

    NewsDTO news(Long id);

    public NewsPostDTO newsPost(Long newsId);

}
