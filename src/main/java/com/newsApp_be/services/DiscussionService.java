package com.newsApp_be.services;

import com.newsApp_be.dto.request.CreateDiscussionDTO;
import com.newsApp_be.dto.response.DiscussionResponseDTO;
import com.newsApp_be.entity.Discussion;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface DiscussionService {
    ResponseEntity<?> createDiscussion(CreateDiscussionDTO create);
    ResponseEntity<?> listDiscussion(Long postId, Pageable pageable);

    DiscussionResponseDTO mapDiscussion(Discussion discussion);
}
