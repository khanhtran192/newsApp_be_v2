package com.newsApp_be.services;

import com.newsApp_be.dto.request.CreateCommentDTO;
import com.newsApp_be.dto.response.CommentDTO;
import com.newsApp_be.entity.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {

    CommentDTO createComment(CreateCommentDTO createCommentDTO);

    CommentDTO mapContenDTO(Comment comment);

    List<CommentDTO> listComment(Long id);

    CommentDTO commentResponse(Long id);

    Comment comment(Long id);
}
