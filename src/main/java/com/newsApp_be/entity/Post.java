package com.newsApp_be.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name="post")
public class Post implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="content")
    private String content;

    @Column(name="createdtime")
    private Date createdTime;

    @Column(name = "link")
    private String link;

    @Column(name = "newsId")
    private Long newsId ;

    @Column(name = "image")
    private String image;

    @ManyToOne
    @JoinColumn(name="User",nullable = false)
    private User userPost;

    @JsonIgnore
    @OneToMany(mappedBy = "post",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Discussion> listDiscussion;

}
