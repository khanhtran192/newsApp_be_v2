package com.newsApp_be.repository;

import com.newsApp_be.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
    Optional<User> findByUsername(String username);
    Optional<User> findByEmailAndEnable(String email, Boolean enable);

    Optional<User> findByUsernameAndEnable(String username, Boolean enable);

    Boolean existsByEmailAndEnable(String email, Boolean enable);
    Boolean existsByUsernameAndEmailAndEnable(String username, String email, Boolean enable);

    Boolean existsByUsernameAndEnable(String username, Boolean enable);
}
