package com.newsApp_be.repository;


import com.newsApp_be.entity.News;
import com.newsApp_be.entity.NewsCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
    @Query(value = "SELECT n FROM News n WHERE n.status = (:status)")
    Page<News> listNews(@Param("status") String status, Pageable pageable);

    @Query(value = "SELECT n FROM News n WHERE n.status = (:status) AND n.category = (:category)")
    Page<News> listNewsByCategory(@Param("status") String status, NewsCategory category
            , Pageable pageable);

    @Query(value = "SELECT n FROM News n WHERE n.status = (:status)")
    Page<News> listNewsTrending(@Param("status") String status, Pageable pageable);

    @Query(value = "SELECT n FROM News n WHERE n.status = (:status) AND n.category = (:category)")
    Page<News> listNewsTrendingByCategory(@Param("status") String status,@Param("category") NewsCategory category
            , Pageable pageable);
}
