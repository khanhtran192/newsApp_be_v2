package com.newsApp_be.repository;

import com.newsApp_be.entity.Comment;
import com.newsApp_be.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "SELECT c FROM Comment c WHERE c.news = (:news) ORDER BY c.createdTime DESC")
    List<Comment> listComment(@Param("news") News news);

    List<Comment> findByNews(@Param("news") News news);
}
