package com.newsApp_be.repository;

import com.newsApp_be.entity.Discussion;
import com.newsApp_be.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiscussionRepository extends JpaRepository<Discussion, Long> {
    List<Discussion> findDiscussionByPost(Post post);

    @Query(value = "SELECT d FROM Discussion d WHERE d.post = (:post)")
    Page<Discussion> findByPost(@Param("post") Post post, Pageable pageable);
}
