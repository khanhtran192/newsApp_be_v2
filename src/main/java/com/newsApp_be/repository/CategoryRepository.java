package com.newsApp_be.repository;

import com.newsApp_be.entity.News;
import com.newsApp_be.entity.NewsCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<NewsCategory, Long> {
    @Query(value = "SELECT c " +
            "FROM NewsCategory c " +
            "JOIN " +
            "News n " +
            "ON c.id = n.category.id " +
            "GROUP BY c.id " +
            "ORDER BY COUNT(n.id) DESC ")
    Page<NewsCategory> pageCategory(Pageable pageable);
}
