package com.newsApp_be.repository;

import com.newsApp_be.entity.Comment;
import com.newsApp_be.entity.ReComment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecommentRepository extends JpaRepository<ReComment, Long> {

    List<ReComment> findByComment(Comment comment);
}
